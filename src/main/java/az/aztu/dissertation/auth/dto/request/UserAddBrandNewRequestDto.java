package az.aztu.dissertation.auth.dto.request;

import az.aztu.dissertation.auth.enums.UserAuthority;

import java.io.Serializable;
import java.util.List;

public class UserAddBrandNewRequestDto implements Serializable {

    private Long id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private List<UserAuthority> userAuthorityList;

    public Long getId() {
        return id;
    }

    public UserAddBrandNewRequestDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserAddBrandNewRequestDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public UserAddBrandNewRequestDto setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserAddBrandNewRequestDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserAddBrandNewRequestDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public List<UserAuthority> getUserAuthorityList() {
        return userAuthorityList;
    }

    public UserAddBrandNewRequestDto setUserAuthorityList(List<UserAuthority> userAuthorityList) {
        this.userAuthorityList = userAuthorityList;
        return this;
    }
}
