package az.aztu.dissertation.auth.service;

import az.aztu.dissertation.auth.dto.entity.UserDto;
import az.aztu.dissertation.auth.dto.request.UserAddBrandNewRequestDto;
import az.aztu.dissertation.auth.exception.custom.ClientException;
import az.aztu.dissertation.auth.mappers.UserMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static az.aztu.dissertation.auth.enums.Message.ERROR_DUPLICATED_PIN;

@Service
public class AuthRegistrationService {

    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    public AuthRegistrationService(PasswordEncoder passwordEncoder,
                                   UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    public void generateRegistration(UserAddBrandNewRequestDto userAddBrandNewRequest) {
        userAddBrandNewRequest.setPassword(
                passwordEncoder.encode(userAddBrandNewRequest.getPassword())
        );
        // username cannot be duplicated
        validateDuplicatedUsername(userAddBrandNewRequest.getUsername());
        userService.insertOrUpdate(
                UserMapper.INSTANCE.fromRequestToDto(userAddBrandNewRequest)
        );
    }

    private void validateDuplicatedUsername(String username) {
        UserDto userDto = userService.getByUsername(username);
        if(userDto != null
           && userDto.getId() != null) {
           throw new ClientException(ERROR_DUPLICATED_PIN.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
