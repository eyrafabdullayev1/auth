package az.aztu.dissertation.auth.service;

import az.aztu.dissertation.auth.dto.entity.UserDto;
import az.aztu.dissertation.auth.entity.UserDetailsEntity;
import az.aztu.dissertation.auth.entity.UserEntity;
import az.aztu.dissertation.auth.mappers.UserMapper;
import az.aztu.dissertation.auth.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDto getById(Long id) {
        return userRepository.findById(id)
                             .map(UserMapper.INSTANCE::toDto)
                             .orElseGet(UserDto::new);
    }

    public UserDto getByUsername(String username) {
        return userRepository.findByUsername(username)
                             .map(UserMapper.INSTANCE::toDto)
                             .orElseGet(UserDto::new);
    }

    public UserDto insertOrUpdate(UserDto userDto) {
        UserEntity userEntity = UserMapper.INSTANCE.toEntity(userDto);
        for(UserDetailsEntity userDetailsEntity : userEntity.getUserAuthorityList()) {
            userDetailsEntity.setRelatedUser(userEntity);
        }
        return UserMapper.INSTANCE.toDto(
                userRepository.save(userEntity)
        );
    }
}
