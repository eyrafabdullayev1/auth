package az.aztu.dissertation.auth.enums;

public enum UserAuthority {

    USER, ADMIN
}
