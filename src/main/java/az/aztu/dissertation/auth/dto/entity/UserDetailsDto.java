package az.aztu.dissertation.auth.dto.entity;

import az.aztu.dissertation.auth.enums.UserAuthority;

import java.io.Serializable;

public class UserDetailsDto implements Serializable {

    private Long id;
    private UserAuthority userAuthority;
    private UserDto relatedUser;

    public Long getId() {
        return id;
    }

    public UserDetailsDto setId(Long id) {
        this.id = id;
        return this;
    }

    public UserAuthority getUserAuthority() {
        return userAuthority;
    }

    public UserDetailsDto setUserAuthority(UserAuthority userAuthority) {
        this.userAuthority = userAuthority;
        return this;
    }

    public UserDto getRelatedUser() {
        return relatedUser;
    }

    public UserDetailsDto setRelatedUser(UserDto relatedUser) {
        this.relatedUser = relatedUser;
        return this;
    }
}
