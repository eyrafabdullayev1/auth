package az.aztu.dissertation.auth.dto.request;

import java.io.Serializable;

public class AuthenticationTokenRequestDto implements Serializable {

    private String username;
    private String password;

    public AuthenticationTokenRequestDto() {
    }

    public String getUsername() {
        return this.username;
    }

    public AuthenticationTokenRequestDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return this.password;
    }

    public AuthenticationTokenRequestDto setPassword(String password) {
        this.password = password;
        return this;
    }
}
