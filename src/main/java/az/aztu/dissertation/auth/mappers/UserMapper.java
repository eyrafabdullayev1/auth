package az.aztu.dissertation.auth.mappers;

import az.aztu.dissertation.auth.dto.entity.UserDetailsDto;
import az.aztu.dissertation.auth.dto.entity.UserDto;
import az.aztu.dissertation.auth.dto.request.UserAddBrandNewRequestDto;
import az.aztu.dissertation.auth.entity.UserDetailsEntity;
import az.aztu.dissertation.auth.entity.UserEntity;
import az.aztu.dissertation.auth.enums.UserAuthority;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto toDto(UserEntity entity);

    default UserDetailsDto userDetailsEntityToUserDetailsDto(UserDetailsEntity userDetailsEntity) {
        if ( userDetailsEntity == null ) {
            return null;
        }

        UserDetailsDto userDetailsDto = new UserDetailsDto();

        userDetailsDto.setId( userDetailsEntity.getId() );
        userDetailsDto.setUserAuthority( userDetailsEntity.getUserAuthority() );
        userDetailsDto.setRelatedUser(
                userDetailsEntity.getRelatedUser() != null
                && userDetailsEntity.getRelatedUser().getId() != null
                        ? new UserDto().setId(userDetailsEntity.getRelatedUser().getId())
                        : new UserDto()
        );

        return userDetailsDto;
    }

    UserEntity toEntity(UserDto dto);

    default UserDetailsEntity userDetailsDtoToUserDetailsEntity(UserDetailsDto userDetailsDto) {
        if ( userDetailsDto == null ) {
            return null;
        }

        UserDetailsEntity userDetailsEntity = new UserDetailsEntity();

        userDetailsEntity.setId( userDetailsDto.getId() );
        userDetailsEntity.setUserAuthority( userDetailsDto.getUserAuthority() );
        userDetailsEntity.setRelatedUser(
                userDetailsDto.getRelatedUser() != null
                && userDetailsDto.getRelatedUser().getId() != null
                        ? new UserEntity().setId(userDetailsEntity.getRelatedUser().getId())
                        : new UserEntity()
        );

        return userDetailsEntity;
    }

    default UserDto fromRequestToDto(UserAddBrandNewRequestDto requestDto) {
        if(requestDto == null) {
            return null;
        }

        UserDto userDto = new UserDto();
        userDto.setName(requestDto.getName());
        userDto.setSurname(requestDto.getSurname());
        userDto.setUsername(requestDto.getUsername());
        userDto.setPassword(requestDto.getPassword());

        List<UserAuthority> userAuthorityList = requestDto.getUserAuthorityList();
        if(userAuthorityList != null && !userAuthorityList.isEmpty()) {
            List<UserDetailsDto> userDetailsDtoList =
                    userAuthorityList.stream()
                                     .map((a) -> new UserDetailsDto().setUserAuthority(a))
                                     .collect(Collectors.toList());
            userDto.setUserAuthorityList(userDetailsDtoList);
        }

        return userDto;
    }
}
