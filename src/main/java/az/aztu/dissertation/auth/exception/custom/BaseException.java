package az.aztu.dissertation.auth.exception.custom;

public class BaseException extends RuntimeException {

    public BaseException() {

    }

    public BaseException(String message) {
        super(message);
    }
}
