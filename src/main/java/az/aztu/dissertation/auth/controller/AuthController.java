package az.aztu.dissertation.auth.controller;

import az.aztu.dissertation.auth.dto.request.AuthenticationTokenRequestDto;
import az.aztu.dissertation.auth.dto.request.UserAddBrandNewRequestDto;
import az.aztu.dissertation.auth.dto.response.CommonResponse;
import az.aztu.dissertation.auth.service.AuthLoginService;
import az.aztu.dissertation.auth.service.AuthRegistrationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static az.aztu.dissertation.auth.enums.ErrorMessage.SUCCESS;
import static az.aztu.dissertation.auth.enums.Message.SUCCESS_USER_REGISTERED;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final AuthLoginService authLoginService;
    private final AuthRegistrationService authRegistrationService;

    public AuthController(AuthLoginService authLoginService,
                          AuthRegistrationService authRegistrationService) {
        this.authLoginService = authLoginService;
        this.authRegistrationService = authRegistrationService;
    }

    @PostMapping("/register")
    public ResponseEntity<?> authRegister(
            @RequestBody UserAddBrandNewRequestDto userAddBrandNewRequest
    ) {
        authRegistrationService.generateRegistration(userAddBrandNewRequest);
        return new CommonResponse<>().success(
                SUCCESS.getMessageBody(),
                null
        );
    }

    @PostMapping("/login")
    public ResponseEntity<?> authLogin(
            @RequestBody AuthenticationTokenRequestDto authenticationTokenRequest
    ) {
        return new CommonResponse<>().success(
                SUCCESS_USER_REGISTERED.getMessage(),
                authLoginService.prepareToken(authenticationTokenRequest)
        );
    }
}
