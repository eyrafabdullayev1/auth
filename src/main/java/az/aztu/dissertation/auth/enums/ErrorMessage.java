package az.aztu.dissertation.auth.enums;

public enum ErrorMessage {

    ERROR_USER_NOT_FOUND("User or password is not correct"),
    ERROR_BAD_REQUEST("Make sure data is right"),
    ERROR_EXTERNAL_SERVICE_UNAVAILABLE("External service unavailable"),
    SUCCESS("Operation was successful");

    private final String messageBody;

    ErrorMessage(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageBody() {
        return this.messageBody;
    }
}
