package az.aztu.dissertation.auth.service;

import az.aztu.dissertation.auth.dto.request.AuthenticationTokenRequestDto;
import az.aztu.dissertation.auth.exception.custom.AuthenticationException;
import az.aztu.dissertation.auth.util.TokenUtil;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Objects;

import static az.aztu.dissertation.auth.enums.Message.ERROR_INVALID_CREDENTIALS;
import static az.aztu.dissertation.auth.enums.Message.ERROR_USER_DISABLED;

@Service
public class AuthLoginService {

    private final TokenUtil tokenUtil;
    private final UserDetailsService userDetailsService;
    private final AuthenticationManager authenticationManager;

    public AuthLoginService(TokenUtil tokenUtil,
                            UserDetailsService userDetailsService,
                            AuthenticationManager authenticationManager) {
        this.tokenUtil = tokenUtil;
        this.userDetailsService = userDetailsService;
        this.authenticationManager = authenticationManager;
    }

    public String prepareToken(AuthenticationTokenRequestDto authenticationTokenRequest) {
        authenticate(authenticationTokenRequest.getUsername(), authenticationTokenRequest.getPassword());
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationTokenRequest.getUsername());
        return tokenUtil.generateToken(userDetails);
    }

    private void authenticate(String username, String password) {
        Objects.requireNonNull(username);
        Objects.requireNonNull(password);

        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException ex) {
            throw new AuthenticationException(ERROR_USER_DISABLED.getMessage(), HttpStatus.FORBIDDEN);
        } catch (BadCredentialsException ex) {
            throw new AuthenticationException(ERROR_INVALID_CREDENTIALS.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
